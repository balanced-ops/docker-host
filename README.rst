docker-host
===========

mac os x
--------

Install docker client on macos x 

- ``brew install docker``
- ``vagrant up --provision``

When done provisioning, ``docker login`` to set credentials.

centos
------

Sometimes you want to test on an CentOS image, let's say you're trying to make sure something works on the Amazon Linux Family [#]_ , so this gives you a centos host.

- ``vagrant up centos-docker-host --provision``


.. [#] Amazon Linux is not CentOS, but it's relatively close-ish to it and I'd say part of the RedHat os family
